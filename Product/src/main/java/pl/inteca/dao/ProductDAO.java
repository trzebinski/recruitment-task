package pl.inteca.dao;

import pl.inteca.dto.Product;

import java.util.List;

public interface ProductDAO {

    List<Product> getProducts(List<String> accounts);

    void createProduct(Product product);
}
