package pl.inteca.dao;

import org.springframework.stereotype.Component;
import pl.inteca.db.DBDriver;
import pl.inteca.db.DBHelper;
import pl.inteca.dto.Product;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ProductDAOImpl implements ProductDAO {

    private final DBDriver dbDriver;
    private DBHelper dbHelper;

    public ProductDAOImpl(DBDriver dbDriver) {
        this.dbDriver = dbDriver;
    }

    @PostConstruct
    public void init() {
        dbHelper = new DBHelper(dbDriver.getConnection());
    }

    @Override
    public List<Product> getProducts(List<String> accounts) {
        return dbHelper.getProductsDB(accounts);
    }

    @Override
    public void createProduct(Product product) {
        dbHelper.createProductDB(product);
    }
}
