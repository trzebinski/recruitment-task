package pl.inteca.service;

import org.springframework.stereotype.Component;
import pl.inteca.dao.ProductDAO;
import pl.inteca.dto.Product;

import java.util.List;

@Component
public class ProductServiceImpl implements ProductService {

    private final ProductDAO productDAO;

    public ProductServiceImpl(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Override
    public List<Product> getProducts(List<String> accounts) {
        return productDAO.getProducts(accounts);
    }

    @Override
    public void createProduct(Product product) {
        productDAO.createProduct(product);
    }
}
