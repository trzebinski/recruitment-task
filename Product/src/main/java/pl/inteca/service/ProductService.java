package pl.inteca.service;

import pl.inteca.dto.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts(List<String> accounts);

    void createProduct(Product product);
}
