package pl.inteca.controller;

import org.springframework.web.bind.annotation.*;
import pl.inteca.dto.Product;
import pl.inteca.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/Product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/GetProducts")
    public List<Product> getProducts(@RequestParam List<String> accounts) {
        return productService.getProducts(accounts);
    }

    @PostMapping("/CreateProduct")
    public void createProduct(@RequestBody Product product) {
        productService.createProduct(product);
    }
}
