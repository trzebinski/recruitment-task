package pl.inteca.db;

import pl.inteca.dto.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    private final Connection connection;

    public DBHelper(final Connection connection) throws NullPointerException {
        if (connection == null)
            throw new NullPointerException();
        this.connection = connection;
    }

    private Product parseProduct(ResultSet resultSet) throws SQLException {
        final int creditID = resultSet.getInt("CreditID");
        final String productName = resultSet.getString("ProductName");
        final int value = resultSet.getInt("Value");

        return new Product(creditID, productName, value);
    }

    // Get all products from Product table
    public List<Product> getProductsDB(List<String> accounts) {
        String query = "SELECT * FROM \"ProductDB\".\"Product\" WHERE \"CreditID\" = ANY (?);";

        List<Product> productsList = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setArray(1, connection.createArrayOf("int", accounts.toArray()));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                productsList.add(parseProduct(resultSet));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productsList;
    }

    // Insert new product into Product table
    public void createProductDB(Product product) {
        String query = "INSERT INTO \"ProductDB\".\"Product\"(\"CreditID\", \"ProductName\", \"Value\")VALUES (?, ?, ?);";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, product.getCreditID());
            preparedStatement.setString(2, product.getProductName());
            preparedStatement.setInt(3, product.getValue());

            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
