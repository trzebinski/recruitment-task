--------------------------------- Credit ---------------------------------

CREATE SCHEMA IF NOT EXISTS "CreditDB" AUTHORIZATION postgres;

CREATE TABLE IF NOT EXISTS "CreditDB"."Credit"
(
    "CreditName" character varying COLLATE pg_catalog."default" NOT NULL,
    "ID" integer NOT NULL,
    CONSTRAINT "PK_Credit" PRIMARY KEY ("ID")
)
TABLESPACE pg_default;

-------------------------------- Customer --------------------------------

CREATE SCHEMA IF NOT EXISTS "CustomerDB" AUTHORIZATION postgres;

CREATE TABLE IF NOT EXISTS "CustomerDB"."Customer"
(
    "CreditID" integer NOT NULL,
    "FirstName" character varying COLLATE pg_catalog."default" NOT NULL,
    "Pesel" character varying COLLATE pg_catalog."default" NOT NULL,
    "Surname" character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "PK_Customer" PRIMARY KEY ("CreditID")
)
TABLESPACE pg_default;

-------------------------------- Product ---------------------------------

CREATE SCHEMA IF NOT EXISTS "ProductDB" AUTHORIZATION postgres;

CREATE TABLE IF NOT EXISTS "ProductDB"."Product"
(
    "CreditID" integer NOT NULL,
    "ProductName" character varying COLLATE pg_catalog."default" NOT NULL,
    "Value" integer NOT NULL,
    CONSTRAINT "PK_Product" PRIMARY KEY ("CreditID")
)
TABLESPACE pg_default;
