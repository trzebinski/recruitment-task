package pl.inteca.dao;

import org.springframework.stereotype.Component;
import pl.inteca.db.DBDriver;
import pl.inteca.db.DBHelper;
import pl.inteca.dto.Credit;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class CreditDAOImpl implements CreditDAO {

    private final DBDriver dbDriver;
    private DBHelper dbHelper;

    public CreditDAOImpl(DBDriver dbDriver) {
        this.dbDriver = dbDriver;
    }

    @PostConstruct
    public void init() {
        dbHelper = new DBHelper(dbDriver.getConnection());
    }

    @Override
    public List<Credit> getCredits() {
        return dbHelper.getCreditsDB();
    }

    @Override
    public void createCredit(Credit credit) {
        dbHelper.createCreditDB(credit);
    }
}
