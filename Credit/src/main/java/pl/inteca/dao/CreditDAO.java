package pl.inteca.dao;

import pl.inteca.dto.Credit;

import java.util.List;

public interface CreditDAO {

    List<Credit> getCredits();

    void createCredit(Credit credit);
}
