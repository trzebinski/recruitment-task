package pl.inteca.controller;

import org.springframework.web.bind.annotation.*;
import pl.inteca.model.RequestModel;
import pl.inteca.service.CreditService;

import java.util.ArrayList;

@RestController
@RequestMapping("/Credit")
public class CreditController {

    private final CreditService creditService;

    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }

    @GetMapping("/GetCredits")
    public ArrayList<RequestModel> getCredits() {
        return creditService.getCredits();
    }

    @PostMapping("/CreateCredit")
    public int createCredit(@RequestBody RequestModel requestModel) {
        return creditService.createCredit(requestModel);
    }
}
