package pl.inteca.db;

import pl.inteca.dto.Credit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    private final Connection connection;

    public DBHelper(final Connection connection) throws NullPointerException {
        if (connection == null)
            throw new NullPointerException();
        this.connection = connection;
    }

    private Credit parseCredit(ResultSet resultSet) throws SQLException {
        final int id = resultSet.getInt("ID");
        final String creditName = resultSet.getString("creditName");

        return new Credit(id, creditName);
    }

    // Get all credits from Credit table
    public List<Credit> getCreditsDB() {
        String query = "SELECT * FROM \"CreditDB\".\"Credit\"";

        List<Credit> creditsList = new ArrayList();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                creditsList.add(parseCredit(resultSet));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return creditsList;
    }

    // Insert new credit into Credit table
    public void createCreditDB(Credit credit) {
        String query = "INSERT INTO \"CreditDB\".\"Credit\"( \"CreditName\", \"ID\") VALUES (?, ?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, credit.getCreditName());
            preparedStatement.setInt(2, credit.getId());

            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
