package pl.inteca.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class DBDriver {

    private Connection connection;

    @Value("${postgres.ip}")
    private String ip;

    @Value("${postgres.user}")
    private String user;

    @Value("${postgres.password}")
    private String password;

    // Method that initiates connection to database
    @PostConstruct
    public void init() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://" + ip + ":5432/", user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
