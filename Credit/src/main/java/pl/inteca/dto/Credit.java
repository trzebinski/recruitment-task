package pl.inteca.dto;

import java.util.Random;

public class Credit {

    private int id;
    private String creditName;

    public Credit() {
    }

    public Credit(String creditName) {
        this.id = generateCreditNumber();
        this.creditName = creditName;
    }

    public Credit(int id, String creditName) {
        this.id = id;
        this.creditName = creditName;
    }

    // Method to generate unique credit number
    private int generateCreditNumber() {
        return new Random(System.nanoTime()).nextInt(999999999);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreditName() {
        return creditName;
    }

    public void setCreditName(String creditName) {
        this.creditName = creditName;
    }
}
