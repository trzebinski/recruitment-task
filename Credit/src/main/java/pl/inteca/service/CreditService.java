package pl.inteca.service;

import pl.inteca.model.RequestModel;

import java.util.ArrayList;

public interface CreditService {

    ArrayList<RequestModel> getCredits();

    int createCredit(RequestModel requestModel);
}
