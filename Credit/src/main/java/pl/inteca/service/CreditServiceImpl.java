package pl.inteca.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.inteca.dao.CreditDAO;
import pl.inteca.dto.Credit;
import pl.inteca.dto.Customer;
import pl.inteca.dto.Product;
import pl.inteca.model.RequestModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

@Component
public class CreditServiceImpl implements CreditService {

    @Value("${customer.ip}")
    private String customerIp;

    @Value("${product.ip}")
    private String productIp;

    private final CreditDAO creditDAO;

    public CreditServiceImpl(CreditDAO creditDAO) {
        this.creditDAO = creditDAO;
    }

    @Override
    public ArrayList<RequestModel> getCredits() {
        List<Credit> creditsList = creditDAO.getCredits();
        List<Integer> accounts = new ArrayList<>();

        for (Credit credit : creditsList)
            accounts.add(credit.getId());

        RestTemplate restTemplate = new RestTemplate();

        String values = String.join(",", accounts.toString()).replace("[", "").replace("]", "");

        // Get all customers from the Customer component
        Customer[] customers = restTemplate.getForObject(
                "http://" + customerIp + ":8081/Customer/GetCustomers?accounts={values}",
                Customer[].class,
                values);

        List<Customer> customersList = Arrays.asList(customers);

        // Get all products from the Product component
        Product[] products = restTemplate.getForObject(
                "http://" + productIp + ":8082/Product/GetProducts?accounts={values}",
                Product[].class,
                values);

        List<Product> productsList = Arrays.asList(products);

        ArrayList<RequestModel> requestModels = new ArrayList<>();

        // Lists aggregation using TreeMap
        TreeMap<Integer, RequestModel> treeMap = new TreeMap<>();

        for (Customer customer : customersList)
            treeMap.put(customer.getCreditID(), new RequestModel(customer.getFirstName(),
                    customer.getSurname(),
                    customer.getPesel(), null, 0, null));

        for (Product product : productsList) {
            RequestModel requestModel = treeMap.get(product.getCreditID());
            requestModel.setProductName(product.getProductName());
            requestModel.setValue(product.getValue());
        }

        for (Credit credit : creditsList) {
            RequestModel requestModel = treeMap.get(credit.getId());
            requestModel.setCreditName(credit.getCreditName());
        }

        treeMap.forEach((key, value) ->
                requestModels.add(value)
        );

        return requestModels;
    }

    @Override
    public int createCredit(RequestModel requestModel) {
        Credit credit = new Credit(requestModel.getCreditName());

        RestTemplate restTemplate = new RestTemplate();

        Customer customer = new Customer(
                credit.getId(),
                requestModel.getName(),
                requestModel.getPesel(),
                requestModel.getSurname());

        // Submit new customer to Customer component
        restTemplate.postForEntity("http://" + customerIp + ":8081/Customer/CreateCustomer",
                customer, Customer.class);

        Product product = new Product(
                credit.getId(),
                requestModel.getProductName(),
                requestModel.getValue());

        // Submit new product to Product component
        restTemplate.postForEntity("http://" + productIp + ":8082/Product/CreateProduct",
                product, Product.class);

        creditDAO.createCredit(credit);

        return credit.getId();
    }
}
