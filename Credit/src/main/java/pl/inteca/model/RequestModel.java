package pl.inteca.model;

public class RequestModel {

    private String name;
    private String surname;
    private String pesel;
    private String productName;
    private int value;
    private String creditName;

    public RequestModel(String name, String surname, String pesel,
                        String productName, int value, String creditName) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.productName = productName;
        this.value = value;
        this.creditName = creditName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCreditName() {
        return creditName;
    }

    public void setCreditName(String creditName) {
        this.creditName = creditName;
    }
}
