# Stop running containers
docker stop postgres
docker stop product
docker stop customer
docker stop credit

# Remove images
docker rmi postgres-img
docker rmi product-img
docker rmi customer-img
docker rmi credit-img
