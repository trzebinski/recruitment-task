# Recruitment task

Recruitment task for Junior Java Developer.

Technologies
---
Project is created with: Java, Spring Boot, PostgreSQL, Docker, Maven, Postman, REST API

Getting Started (on Linux)
---
To manage containers without a ROOT user, you must:

- Create the docker group: 

        sudo groupadd docker

- Add your user to the docker group: 

        sudo usermod -aG docker $USER

- Run the following command to activate the changes to groups:

        newgrp docker

In home directory, run the following command to download project:

    git clone https://trzebinski@bitbucket.org/trzebinski/recruitment-task.git

In downloaded directory, run the following script to start containers:

    ./start.sh

In downloaded directory, run the following script to stop containers:

    ./stop.sh

Screenshot examples
---
To present the implemented services used Postman application.

- CreateCredit

![CreateCredit](https://i.ibb.co/NNCfy71/Create-Credit.png)

- GetCredits

![GetCredits](https://i.ibb.co/jRx5Sb0/Get-Credits.png)
