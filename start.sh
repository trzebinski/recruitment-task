# Build and run container with database
docker build -t postgres-img ./PostgreSQL
docker run -d -p 5432:5432 --name postgres --rm postgres-img

# Get IP for database
POSTGRES_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres)

# Add IP to configuration file
sed -i -e "s/^postgres.ip=.*$/postgres.ip=$POSTGRES_IP/g" ./Product/src/main/resources/application.properties
sed -i -e "s/^postgres.ip=.*$/postgres.ip=$POSTGRES_IP/g" ./Customer/src/main/resources/application.properties
sed -i -e "s/^postgres.ip=.*$/postgres.ip=$POSTGRES_IP/g" ./Credit/src/main/resources/application.properties

# Build Product component
mvn -f ./Product/pom.xml clean install

# Build and run container with Product component
docker build -t product-img ./Product
docker run -d -p 8082:8082 --name product --rm product-img

# Build Customer component
mvn -f ./Customer/pom.xml clean install

# Build and run container with Customer component
docker build -t customer-img ./Customer
docker run -d -p 8081:8081 --name customer --rm customer-img

# Get IP for components
PRODUCT_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' product)
CUSTOMER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' customer)

# Add IP to configuration file
sed -i -e "s/^product.ip=.*$/product.ip=$PRODUCT_IP/g" ./Credit/src/main/resources/application.properties
sed -i -e "s/^customer.ip=.*$/customer.ip=$CUSTOMER_IP/g" ./Credit/src/main/resources/application.properties

# Build Credit component
mvn -f ./Credit/pom.xml clean install

# Build and run container with Credit component
docker build -t credit-img ./Credit
docker run -d -p 8080:8080 --name credit --rm credit-img
