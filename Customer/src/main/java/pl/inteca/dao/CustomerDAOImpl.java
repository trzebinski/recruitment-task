package pl.inteca.dao;

import org.springframework.stereotype.Component;
import pl.inteca.db.DBDriver;
import pl.inteca.db.DBHelper;
import pl.inteca.dto.Customer;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class CustomerDAOImpl implements CustomerDAO {

    private final DBDriver dbDriver;
    private DBHelper dbHelper;

    public CustomerDAOImpl(DBDriver dbDriver) {
        this.dbDriver = dbDriver;
    }

    @PostConstruct
    public void init() {
        dbHelper = new DBHelper(dbDriver.getConnection());
    }

    @Override
    public List<Customer> getCustomers(List<String> accounts) {
        return dbHelper.getCustomersDB(accounts);
    }

    @Override
    public void createCustomer(Customer customer) {
        dbHelper.createCustomerDB(customer);
    }
}
