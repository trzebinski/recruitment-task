package pl.inteca.dao;

import pl.inteca.dto.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> getCustomers(List<String> accounts);

    void createCustomer(Customer customer);
}
