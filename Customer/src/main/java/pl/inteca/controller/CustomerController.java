package pl.inteca.controller;

import org.springframework.web.bind.annotation.*;
import pl.inteca.dto.Customer;
import pl.inteca.service.CustomerServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/Customer")
public class CustomerController {

    private CustomerServiceImpl customerService;

    public CustomerController(CustomerServiceImpl customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/GetCustomers")
    public List<Customer> getCustomers(@RequestParam List<String> accounts) {
        return customerService.getCustomers(accounts);
    }

    @PostMapping("/CreateCustomer")
    public void createCustomer(@RequestBody Customer customer) {
        customerService.createCustomer(customer);
    }
}
