package pl.inteca.db;

import pl.inteca.dto.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    private final Connection connection;

    public DBHelper(final Connection connection) throws NullPointerException {
        if (connection == null)
            throw new NullPointerException();
        this.connection = connection;
    }

    private Customer parseCustomer(ResultSet resultSet) throws SQLException {
        final int creditID = resultSet.getInt("CreditID");
        final String firstName = resultSet.getString("FirstName");
        final String pesel = resultSet.getString("Pesel");
        final String surname = resultSet.getString("Surname");

        return new Customer(creditID, firstName, pesel, surname);
    }

    // Get all customers from Customer table
    public List<Customer> getCustomersDB(List<String> accounts) {
        String query = "SELECT * FROM \"CustomerDB\".\"Customer\" WHERE \"CreditID\" = ANY (?);";

        List<Customer> customersList = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setArray(1, connection.createArrayOf("int", accounts.toArray()));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersList.add(parseCustomer(resultSet));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customersList;
    }

    // Insert new customer into Customer table
    public void createCustomerDB(Customer customer) {
        String query = "INSERT INTO \"CustomerDB\".\"Customer\"(\"CreditID\", \"FirstName\", \"Pesel\", \"Surname\") VALUES (?, ?, ?, ?);";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, customer.getCreditID());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getPesel());
            preparedStatement.setString(4, customer.getSurname());

            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
