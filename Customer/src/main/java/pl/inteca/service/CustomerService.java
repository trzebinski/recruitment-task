package pl.inteca.service;

import pl.inteca.dto.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers(List<String> accounts);

    void createCustomer(Customer customer);
}
