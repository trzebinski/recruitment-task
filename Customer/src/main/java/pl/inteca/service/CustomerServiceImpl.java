package pl.inteca.service;

import org.springframework.stereotype.Component;
import pl.inteca.dao.CustomerDAO;
import pl.inteca.dto.Customer;

import java.util.List;

@Component
public class CustomerServiceImpl implements CustomerService {

    private final CustomerDAO customerDAO;

    public CustomerServiceImpl(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Override
    public List<Customer> getCustomers(List<String> accounts) {
        return customerDAO.getCustomers(accounts);
    }

    @Override
    public void createCustomer(Customer customer) {
        customerDAO.createCustomer(customer);
    }
}
